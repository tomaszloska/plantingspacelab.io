const feed_options = {
  canonical_base: 'https://plantingspace.gitlab.io',
};

module.exports = {
  title: 'PlantingSpace',
  description: 'We build tools that aim to evolve some of the fundamental patterns of how humans interact, discover, and create.',
  themeConfig: {
      nav: [
        { text: 'Taiga', link: '/taiga.md' },
        { text: 'Mangrove', link: 'https://open-reviews.net' },
        { text: 'Broadleaf', link: '/broadleaf.md' },
        { text: 'Team', link: '/team.md' },
        { text: 'Development', link: 'https://gitlab.com/plantingspace' },
      ],
      sidebar: 'auto',
      search: false
  },
  dest: 'public',
  plugins: [
    [ 'feed', feed_options ],
    [
      '@vuepress/blog',
      {
        directories: [
          {
            // Unique ID of current classification
            id: 'post',
            // Target directory
            dirname: '_posts',
            // Path of the `entry page` (or `list page`)
            path: '/post/',
          },
        ],
      },
    ],
  ]
}
