---
sidebar: false
title: Team
members:
- name: Peter Czaban
  picture: peter.jpg
  links: 
  - title: Website
    link: https://keorn.org
- name: Dina Carabas
  picture: dina.jpg
  links: 
  - title: LinkedIn
    link: https://www.linkedin.com/in/dina-carabas-27195214/)  
  - title: Twitter
    link: https://twitter.com/Dina_Carabas
---

<Team />

# Join us!

Want to work on the future of AI along a unique approach? As an early stage startup we are looking to gradually grow our team. Initial hires will focus on research, software development and operations.

If you are interested, get in touch regardless if an open position is listed below, by sending an email to **hello@planting.space**

Our compensation is competitive with potential to receive cash or equity, if you have an idea of your rate, please include it in your email.

## Open positions

### Julia developer

We are looking for a part-time contracting software developer to contribute to open and closed source codebases. Work time will be flexible and tasks will focus on general development, stats or optimization. [Example issue](https://gitlab.com/plantingspace/tasks/-/issues/1) that you could work on.

#### Useful experience
- Backend software engineering
- Scientific computing
- Julia metaprogramming
- Bayesian statistics
- Optimization algorithms

#### Responsibilities
- Implement features based on clear requirements
- Estimate work required for work packages

### Optimization research intern

We are looking for a short term or part time researcher to specify optimization problems and develop algorithms. You will work on theoretically interesting problems with immediate application in planning problems for AI.

#### Useful experience
- Developing heuristics for NP-hard problems
- Framing optimization problems based on real world situations
- Graduate level studies in optimization
- Basic category theory

#### Responsibilities
- Formalize optimization problems
- Specify relaxations with promising properties
- Develop heuristics or approximation algorithms
- Prove properties of algorithms
